# AssetResize

This is a simple addon for the awesome CMS Statamic.

It is designed to resize large image Assets upon upload, to a configurable maximum width in
pixels (default 1920), to save disk space. It is particularly useful when clients are known to
upload 10MP images all the time. __Note: This addon will delete and replace the original files__

## Installation

Installation is the same as all Statamic Addons. Create a new directory under `site/addons` named
`AssetResize` and add these files to that directory.

## Configuration

There is currently only one setting available, `max_width`. This value is the maximum width in
pixels of your Asset images. All images with a width larger than this will be resized to that
width. __The value must be an integer with no units.__

It is set by editing the `default.yaml` file, or creating a new file
`site/settings/addons/asset_resize.yaml`.

## Compatibility

This addon is for Statamic 2, and all actions done within the addon are using native Statamic /
Glide functionality, so it should be fully compatible with any system capable of running
Statamic 2 and using Glide for image manipulations.

## Copyright / Warranty / Yada, Yada, Yada
In short, there is none.

This addon is freeware and open to be used as is, or as a starting point for further customisation
and development. Use it at your own risk, and test it thoroughly before unleashing it upon
unsuspecting clients. If you find any problems with it, please let me know so I can update it
for the benefit of all.
